var express = require('express');
var app = express();
var multer  =   require('multer');
var path = require('path');
var fs = require("fs");
var ip = require("ip");
var js = require('jsdom');
var expreg = require("regex");
var formidable = require('formidable'), util = require('util');
//var ip="160.80.35.91";
var getIp = require('ipware')().get_ip;
var url  = require('url');
const child = require('child_process');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname+'/public'));
require('./funTools.js')();
require('./result.js')();
//Parser per trovare cartella risultati
function parseUrl(url){
	var regex=/\.*\/[0-9]*$/;
	return(url.match(regex));
}

app.route('/').get(function (req, res) {
	res.redirect('/home');
})

app.get('/home', function (req, res) {
	console.log("/home requested");
	file_ex="";
	reqRes;
	endOfExec = false;
	fold=0;
	motif=0;
	textArea = "";
	backPath="";
	boolFasta=false;
	boolExample=false;
	boolFound=false;
	boolMotif=false;
	res.sendFile( __dirname + "/public/" + "home.html" );
	
})

//Variabili globali
var file_ex="";
var valid=true;
var folder;
var reqRes;
var endOfExec = false;

var textArea = "";
var backPath;
//Flag controllo
var boolFasta;
var boolExample;
var boolFound;
var boolMotif;
var user=[];
//Nome cartella risultati
var date;

//"importa" la parte di codice che prepara il file in upload.
//Alloca le risorse
eval(fs.readFileSync('upload.js')+'');

//Fasta upload
app.post('/file_upload', function(req, res){
			debugger;	
	if(user.length==0){ //Se non ci sono esecuzioni in coda...
		date=Date.now();
		user.push([req.headers["x-forwarded-for"], date, 0, 0, false]);
	}
	else{
		var i=0;
		while(i<user.length){
			if(user[i][0]===req.headers["x-forwarded-for"]){ //Se questo utente ha già caricato un file in back..assegna la stessa data
				date=user[i][1];
			
				break;
			}
			i=i+1;			
		}
		if(i==user.length){
				date=Date.now();
				user.push([req.headers["x-forwarded-for"], date, 0, 0, false]);
		}
	}
	console.log("file",date)
	console.log("/file_upload requested");
	boolFasta=true;
	upload(req,res,function(err) {
        if(err) {
		console.log(err);
        	return res.end("Error uploading file.");
        }

        res.end("File is uploaded");
    });
});

//Background
app.post('/back_upload', function(req,res){
	console.log("/back_upload requested");
			debugger;	
	if(user.length==0){ //Se non ci sono esecuzioni in coda...
		date=Date.now();
		user.push([req.headers["x-forwarded-for"], date, 0, 0, true]);
		console.log("backNew",date)
	}
	else{
		var i =0;
		while(i<user.length){
			if(user[i][0]===req.headers["x-forwarded-for"]){ //Se questo utente ha già caricato un file in input..assegna la stessa data
				date=user[i][1];
				user[i][4]=true;
				console.log("backExisting",date);
				break;
			}
			i=i+1;			
		}
		if(i==user.length){
				date=Date.now();
				user.push([req.headers["x-forwarded-for"], date, 0, 0, true]);
				console.log("backAdd",date)
			}
	}

	uploadBack(req,res,function(err){
	if(err){
		console.log(err);
		return("Error while uploading file.");
	}
	res.end("File uploaded");
	});
});

//Start button
app.post('/start', function(req,res){
	console.log("/start requested");
	//Prendo tutte i campi in opzioni
	var wmin;
	var wmax;
	var clean;
	var form = new formidable.IncomingForm();
	var fold, motif;
	var run,temp,smin,smax,cool,sampling,branch,keep,randomS;
	form.parse(req, function (err, fields, files){
		
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TextArea~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		if (!boolFasta){
			date=Date.now();
			textArea=fields["textfield"];

			wmin=fields["windowmin"];
			wmax=fields["windowmax"];
			motif=fields["motifs"];
			clean=fields["clean"];
			fold=fields["fold"];
			temp=fields["temp"];
			smin=fields["smin"];
			smax=fields["smax"];
			cool=fields["cooling"];
			branch=fields["branch"];
			run=fields["run"];
			sampling=fields["sampling"];
			keep=fields["keep"];
			randomS=fields["random"];
			var parameters = '-w '+wmin+' -W '+wmax+' -v t -C '+clean+' -M '+motif+' -s '+smin+' -S '+smax+' -T '+temp+' -r '+cool+' -R '+randomS+' -n '+sampling + ' -b '+branch + ' -k ' + keep;
			folder=date;
			user.push([req.headers["x-forwarded-for"], date, 0, motif,fold, false]);
			var cmd = 'mkdir results/'+folder;
			runBash(cmd,date);
			folder='results/'+folder;
			backPath=folder+'/background';
			fasta="input_file_"+date+".fa";
			//check textarea format
			var nt=new RegExp(/^(a|c|u|t|T|g|A|C|G|U)*/);
			var dot=new RegExp(/^(\(|\)|\.)*/);
			var checker=[];
			var boolQ = true;
			var boolDot = true;
			var valid=false;
			if(textArea[0]=='>'){

				checker=textArea.split('\n');
				checker[checker.length-1]+=(textArea[textArea.length-1]);
				//console.log(checker[checker.length-2].length,checker[checker.length-1].length);
				var basta = checker[3].match(nt);
				//console.log(basta[0].length);
				if(checker[3][0]=='>'){
					boolQ=false;
				}
				else
					if((basta[0].length)==(checker[3].length-1)){
						//console.log(checker[3]);
						boolQ=false; boolDot=false;

					}
					else{ boolDot=true; boolQ=true;}

				for(var i=1; i<(checker.length); i++){

					basta=checker[i].match(nt);
					if(checker[i-1][0]=='>' && ( ((basta[0].length)==(checker[i].length-1)) ||(basta[0].length==(checker[i].length)))){//nucleo - nome
						valid=true;
					}
					else{ 
						basta=checker[i-1].match(nt);
						var punto=checker[i-1].match(dot);
						var testa=checker[i].match(dot);//console.log(test[0]);
						if((testa[0]!='') || (punto[0]!='')){
							if(((basta[0].length)==(checker[i-1].length-1)) && ((testa[0].length==(checker[i].length-1))||(testa[0].length==(checker[i].length)))){//dot - nucleo
								valid=true;
							}
							else{
								if((punto[0].length==(checker[i-1].length-1)) && boolQ){ //qbear - dot
									valid=true;
								}
								else
									if(checker[i][0]=='>'){//nome
										valid=true;
									}
									else{//debugger;
										valid=false;
										break;
									}
							}
						}
						else{
							if(checker[i][0]=='>'){//nome
								valid=true;
							}
							else{//debugger;
								valid=false;
								break;
							}
						}
					}
				}
			}
			
			if(valid){
				debugger;
				//textArea.replace(/\r?\n|\r/g, '\n');
				fs.writeFileSync("uploads/de-"+fasta, textArea);
				runBash('python saveTheTextArea.py uploads/de-'+fasta+' uploads/'+fasta,date);
				var nt=new RegExp(/(a|c|u|g|A|C|G|U)*/g);
				setTimeout ( function() {
					try{
						var f = fs.readFileSync('uploads/'+fasta, 'utf8');
						f=f.split('\n');
						f.pop();
						if(f[3][0]=='>'){
							//Già foldato
							var mv='mv uploads/'+fasta+' '+folder+'/toFold.fa';
							runBash(mv,date);
							//console.log(mv);
							foldEncode('/toFold.fa',fold,folder,false, background,backPath,parameters,date,2);
						}
						else{
							if((boolDot==false) && (boolQ==false)){
								//da foldare
								foldEncode(fasta,fold,folder,false, background,backPath,parameters,date,1);
							}
							else{
								//completo
								runBash('mv uploads/'+fasta+' '+folder+'/'+date+'.fa',date);
								fasta='/'+date+'.fa';
								foldEncode(fasta,fold,folder,false, background,backPath,parameters,date,3);
							}
						}
					}catch (err){ console.log(err);
					}
				}, 2000);
				res.redirect('/loading');
			}
			else {
				res.redirect('/error');
			}
			
		}
		else{	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~FILE UPLOADED~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			wmin=fields["windowmin"];
			wmax=fields["windowmax"];
			motif=fields["motifs"];
			clean=fields["clean"];
			fold=fields["fold"];	
			
			temp=fields["temp"];
			smin=fields["smin"];
			smax=fields["smax"];
			cool=fields["cooling"];
			branch=fields["branch"];
			run=fields["run"];
			sampling=fields["sampling"];
			keep=fields["keep"];
			randomS=fields["random"];
			var boolBack = false;
			var parameters = '-w '+wmin+' -W '+wmax+' -v t -C '+clean+' -M '+motif+' -s '+smin+' -S '+smax+' -T '+temp+' -r '+cool+' -R '+randomS+' -n '+sampling + ' -b '+branch + ' -k ' + keep;
			debugger;
			for(i in user)
				if(req.headers["x-forwarded-for"]==user[i][0]){
					date=user[i][1];
					user[i][3]=motif;
					user[i].push(fold);
					boolBack=user[i][4];
					if(boolBack)
						background='back_file_'+date+'.fa';
				}
			folder=date;
			var cmd = 'mkdir results/'+folder;
			runBash(cmd,date);
			folder='results/'+folder;
			backPath=folder+'/background';
			fasta="input_file_"+date+".fa";
			console.log("fasta",date, boolBack);
			//Check fasta format
			try{//debugger;
				var nt=new expreg(/(a|c|u|t|g|A|T|C|G|U)*/);
				var f = fs.readFileSync('uploads/'+fasta, 'utf8');
				f=f.split('\n');
				f.pop();
				if(f[3][0]=='>'){
					//Già foldato
					var mv='mv uploads/'+fasta+' '+folder+'/toFold.fa';
					runBash(mv,date);
					//console.log(mv);
					foldEncode('/toFold.fa',fold,folder,boolBack, background,backPath,parameters,date,2);
				}
				else{
					if(nt.test(f[3])){
						//da foldare
						foldEncode(fasta,fold,folder,boolBack, background,backPath,parameters,date,1);
					}
					else{
						//completo
						runBash('mv uploads/'+fasta+' '+folder+'/'+date+'.fa',date);
						fasta='/'+date+'.fa';
						foldEncode(fasta,fold,folder,boolBack, background,backPath,parameters,date,3);
					}
				}
			}catch (err){ console.log(err);
			}

			res.redirect('/loading');
		}
		
	});	
});

//Redirect a pagina temporanea
app.route('/wait').get(function(req,res){
	console.log("/wait requested");
	for(i in user)
		if(req.headers["x-forwarded-for"]===user[i][0])
			reqRes=user[i][1];
	//console.log(reqRes);
	return tempResults(reqRes, res,req);
});

//funzione che restituisce una pagina html con il link
function tempResults (reqRes,res,req){
	for(i in user)
		if(req.headers["x-forwarded-for"]===user[i][0])
			reqRes=user[i][1];
	res.write("<html><title>Pre Result Room - Beam Web Server</title><link href='http://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'><link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"images/favicon.ico\" /><link rel=\"stylesheet\" type=\"text/css\" href=\"css/wait.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"css/generale.css\"><header><div id=\"header\" align=\"center\"><img src=\"images/beam.png\" class=\"imghead\" alt=\"logo\" name=\"logo\"/></div><div id=\"navigation\" align=\"center\"><a class=\"menu\" href=\"/home\" target=\"_self\">Home</a> <a class=\"menu\" href=\"/documentation\" target=\"_self\">Documentation</a><a class=\"menu\" href=\"/contacts\" target=\"_self\">Contacts</a> </div></header><body><fieldset><br>Your results will be available at the following link: <a href=\"/results\">/"+reqRes+"</a><br></fieldset></body><footer><div id=\"footer\"><img id=\"pic_footer\" src=\"images/logoTorVergat.png\" alt=\"logoTV\" /><div style=\"padding-top:20px;\"> © 2016, Centre for Molecular Bioinformatics, University of Rome \"Tor Vergata\".</div></div></footer></html>");
	res.end();
}

app.route('/results').get(function(req,res){
	//console.log("/results requested");
	for(i in user)
		if(req.headers["x-forwarded-for"]===user[i][0]){
			reqRes=user[i][1];
			user.splice(i,1);
		}
	//console.log(user);
	if (boolMotif){
		results(res,reqRes)
	}
	else
		res.redirect('/'+reqRes);
	
});


app.route('/loading').get(function(req,res){
	console.log("/loading requested");
	var pos,fld,mo;
	for(i in user){		//debugger;
		//Check indirizzo IP
		if(req.headers["x-forwarded-for"]===user[i][0]){//se esiste
			pos=i;	//salva la posizione
			reqRes=user[i][1];	//salva la data
			//Controllo percentuale di avanzamento
			if (user[i][3]==0 || typeof user[i][3]=='undefined'){
				fs.readdir(__dirname + "/results/"+reqRes, function(err, files){
					if (err) throw err;
					if (files){
						for (x in files){
							var tempor=files[x].indexOf('_run1.search.txt');
							if (tempor>0){						
								mo=parseInt(files[x][tempor-1],10);
								user[i][2]=0;
								user[i].push(mo);
							}
						}
					}
				});

			}else
			mo=user[i][3];
			fld=user[i][4];
		}
	}
	console.log(user, reqRes);
	fs.stat('risultati/'+reqRes+'/benchmark/motifs/'+reqRes+'_m'+mo+'_run1.txt',function(err,stats){
		if(stats){
			user[pos][2]=60;
			app.use("/out",express.static('results/'+reqRes));
			ris(reqRes,fld,mo);
			res.redirect('/'+reqRes);
		}
		else{

			fs.stat('results/'+reqRes+'/error', function(err, stats){
				if(stats)
					getError(res);
				if (err){
					for(i in user)
						if(req.headers["x-forwarded-for"]===user[i][0])
							pos=i;
					

					if(typeof(pos)=='undefined'){
						//res.end();
						//res.redirect('/notFound');
						notFoundPage(res);
					}
						
					else{
						user[pos][2]=20;
						writeInLoading(res,req); 
					}
				}
			});
		}
	});
	
});
//Loading-waiting page ***static***
function writeInLoading(res,req){
	var perc;
	for(i in user)
		if(req.headers["x-forwarded-for"]===user[i][0]){
			reqRes=user[i][1];
			perc=user[i][2];
		}
	res.write("<html><title>Loading - Beam Web Server</title><link href='http://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'><link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"images/favicon.ico\" /><link rel=\"stylesheet\" type=\"text/css\" href=\"css/generale.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"css/loading.css\"><header><div id=\"header\" align=\"center\"><img src=\"images/beam.png\" class=\"imghead\" alt=\"logo\" name=\"logo\"/></div><div id=\"navigation\" align=\"center\"><a class=\"menu\" href=\"/home\" target=\"_self\">Home</a> <a class=\"menu\" href=\"/documentation\" target=\"_self\">Documentation</a><a class=\"menu\" href=\"/contacts\" target=\"_self\">Contacts</a> </div></header><body onload=\"timer=setTimeout(function(){ window.location='/loading';},"+(30*(100-perc))+" )\"><fieldset><br>Your results are "+perc+"% ready..Please wait<br>You will find your results by bookmarking this link: <a class=\"special\" href=\"/"+reqRes+"\">/"+reqRes+"</a><br><p>This page will refresh every "+(30*(100-perc)/1000)+" seconds</p></fieldset><br></fieldset></body><footer><div id=\"footer\"><img id=\"pic_footer\" src=\"images/logoTorVergat.png\" alt=\"logoTV\" /><div style=\"padding-top:20px;\"> © 2016, Centre for Molecular Bioinformatics, University of Rome \"Tor Vergata\".</div></div></footer></html>");
	res.end();
}
//404 error page
app.route('/notFound').get(function(req,res){
	console.log("/notfound requested");
	return(notFoundPage(res));
});
function notFoundPage(res){
	res.sendFile( __dirname + "/public/" + "notFound.html" );
}
//Error Page
app.route('/error').get(function(req,res){
	console.log("/error requested");
	//If error occurs, delete user's IP and data
	for(i in user)
		if(req.headers["x-forwarded-for"]===user[i][0]){
			console.log(req.headers["x-forwarded-for"]);
			reqRes=user[i][1];
			user.splice(i,1);
			console.log(user);
		}
	return(getError(res));
});
function getError(res){
	res.write("<html><title>Error - Beam Web Server</title><link href='http://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'><link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"images/favicon.ico\" /><link rel=\"stylesheet\" type=\"text/css\" href=\"css/generale.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"css/wait.css\"><header><div id=\"header\" align=\"center\"><img src=\"images/beam.png\" class=\"imghead\" alt=\"logo\" name=\"logo\"/></div><div id=\"navigation\" align=\"center\"><a class=\"menu\" href=\"/home\" target=\"_self\">Home</a> <a class=\"menu\" href=\"/documentation\" target=\"_self\">Documentation</a><a class=\"menu\" href=\"/contacts\" target=\"_self\">Contacts</a> </div></header><body><fieldset><br>An error has occurred.<br><ul><li>You have uploaded a wrong file format!!</li><li>Check your file for wrong characters;</li><li>Check your file for random return characters, or if any sequence is broken in 2 or more lines;</li><li>Internal BEAM error</li></ul> Click <a href=\"/home\" />here</a> to go back at the homepage<br></fieldset></body><footer><div id=\"footer\"><img id=\"pic_footer\" src=\"images/logoTorVergat.png\" alt=\"logoTV\" /><div style=\"padding-top:20px;\"> © 2016, Centre for Molecular Bioinformatics, University of Rome \"Tor Vergata\".</div></div></footer></html>");
	res.end();
}

//Download page
app.get('/download',function(req,res){
console.log("/download requested");
	res.sendFile( __dirname + "/public/" + "download.html" );
});
//Doc page
app.get('/documentation',function(req,res){
console.log("/doc requested");
	res.sendFile( __dirname + "/public/" + "documentation.html" );
});
//Contacts page
app.get('/contacts',function(req,res){
console.log("/contacts requested");
	res.sendFile( __dirname + "/public/" + "contacts.html" );
});

app.get('/*.zip', function(req,res){
	for(i in user)
		if(req.headers["x-forwarded-for"]===user[i][0])
			reqRes=user[i][1];
	res.sendFile(__dirname+'/results/'+reqRes+'/'+reqRes+".zip");
});

//Gestione link risultati
app.get('/*', function(req,res){
	var url_parts = url.parse(req.url);
	var evalPage = parseUrl(url_parts.pathname);
	if (evalPage!=null){	
		reqRes = evalPage[0].slice(1);

		if (user.length==0)
			user.push([req.headers["x-forwarded-for"],reqRes]);
		fs.readdir(__dirname + "/results", function(err, files){
			if (err) throw err;
			if (files){
				//console.log(files);
				for (var i in files){
					if (files[i]==reqRes){
						boolFound=true;
						var boolVarna=false;
						var cartella=__dirname+"/results/"+reqRes;
						app.use("/out",express.static(cartella));
						var pos=0,mo=0;
						if(typeof user != undefined)
						for(i in user)
							if(req.headers["x-forwarded-for"]===user[i][0]){
								pos=i;
								mo=user[i][3];
							}
							fs.stat('results/'+reqRes+'/'+reqRes+'_'+mo+'_weblogo.png', function (err,stats){
			
								if(stats){
									if(user[pos] != undefined)
										user[pos][2]=85;
									boolMotif=readdot(cartella+'/'+reqRes+'_motif.txt');
									//console.log("readdot",boolMotif,reqRes);			//~~~~DEBUG~~~~
									if(boolMotif && !boolVarna){
										//user[pos]=90;
										boolVarna=true;
										structureMotif(res,reqRes);
										res.redirect('/wait');
									}
									else	
										writeInLoading(res,req);
								}
								if(err){		
									//if((typeof user[pos] != undefined) && (user[pos].length>2))
									//	user[pos][3]=20;
									//else
										user[pos].push(20);
									writeInLoading(res,req);
								}
							});
							
					}
				}
			if(!boolFound && i==files.length-1){res.redirect('/notFound');}
			}
		});
	
	}
});

//Server up
var server = app.listen(8081, function () {
	var host = server.address().address;
	var port = server.address().port;
	console.log("Listening @ http://%s:%s", ip.address(), port);
})

