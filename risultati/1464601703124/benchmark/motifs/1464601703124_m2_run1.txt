:::::::::	  ENERGY = 10.3  chr1:5$1	120	39	48	3.8072
obb::::::	  ENERGY = 9.8  chr1:4763354-4763474$1	120	32	41	2.6720
:::::::::	  ENERGY = 11.7  chr1:4$1	120	0	9	3.8072
:::::::::	  ENERGY = 10.3  chr1:4764316-4764436$1	120	39	48	3.8072
:::::::::	  ENERGY = 11.7  chr1:4763892-4764012$1	120	0	9	3.8072
:::::::::	  ENERGY = 11.8  chr1:6$1	123	0	9	3.8072
:::::::::	  ENERGY = 11.8  chr1:4764472-4764595$1	123	0	9	3.8072

#otherMatches

#Seq PSSM
AAAAAAAAA	  ENERGY = 10.3  chr1:5$1	su1	39	48	3.8072
UGCUUCUUC	  ENERGY = 9.8  chr1:4763354-4763474$1	su1	32	41	2.6720
cUUaUccUg	  ENERGY = 11.7  chr1:4$1	su1	0	9	3.8072
AAAAAAAAA	  ENERGY = 10.3  chr1:4764316-4764436$1	su1	39	48	3.8072
cUUaUccUg	  ENERGY = 11.7  chr1:4763892-4764012$1	su1	0	9	3.8072
gaUgcaggc	  ENERGY = 11.8  chr1:6$1	su1	0	9	3.8072
gaUgcaggc	  ENERGY = 11.8  chr1:4764472-4764595$1	su1	0	9	3.8072

#PSSM
: : 0.85	o : 0.14	
: : 0.85	b : 0.14	
: : 0.85	b : 0.14	
: : 0.99	
: : 0.99	
: : 0.99	
: : 0.99	
: : 0.99	
: : 0.99	

#score	25.515346412442852
#seq	7
#width	9
#escape	false
#onStep	15000
#minSteps	10000
#maxSteps	15000.0