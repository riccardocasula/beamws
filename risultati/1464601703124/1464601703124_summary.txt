#input=1464601703124
#input_size=7
#background=back_fastb
#background_size=18
#meanLength=120.85714285714286
#meanStructureContent=0.2139372822299652
#bin=21

#motif=1
:a^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#qBEAR
tztzzzzwwwwwwwwwwwwwwwwwwwwwwzzzztt

pvalue=1.1391312843778145E-5
score=134.41603850803327
coverage=0.5714285714285714
fall-out=0.05555555555555555
runtime(s)=3.71

#motif=2
:::::::::

#qBEAR
ttttttttt

pvalue=0.2522179718012675
score=25.515346412442852
coverage=0.875
fall-out=0.6666666666666666
runtime(s)=1.98
