#input=1464173087295
#input_size=5
#background=autoBG
#background_size=251
#meanLength=120.6
#meanStructureContent=0.5407317073170732
#bin=22

#motif=1
eee*******>>>>>>>>>>>>>>>>*********

#qBEAR
zzz*******eeeeeeeeeeeeeeeeaaaaaaa*z

pvalue=0.001335393805790952
score=77.11376741846668
coverage=0.6
fall-out=0.00398406374501992
runtime(s)=3.48

#motif=2
**cccppppppp**c**************dd

#qBEAR
z*zzzxxxxxxxzzzzzbzaaaaaa*zzzzz

pvalue=0.008376877048565934
score=41.2976698788
coverage=0.6
fall-out=0.00398406374501992
runtime(s)=3.96
