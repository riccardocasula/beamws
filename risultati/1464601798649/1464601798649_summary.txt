#input=1464601798649
#input_size=7
#background=back_fastb
#background_size=19
#meanLength=120.85714285714286
#meanStructureContent=0.2139372822299652
#bin=21

#motif=1
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^a:::::

#qBEAR
zzzzwwwwwwwwwwwwwwwwwwwwzzzzzzttttt

pvalue=6.240174760363537E-6
score=166.98340732600002
coverage=0.5714285714285714
fall-out=0.05263157894736842
runtime(s)=4.35

#motif=2
GGGG{EEEEE:::::hhhhhhhhmmmmhhhhhhhh

#qBEAR
ffffgvvvvvtttttaaaaaaaaxxxxaaaaaaaa

pvalue=1.491648082829755E-8
score=70.8182212285
coverage=0.42857142857142855
fall-out=0.05263157894736842
runtime(s)=10.95
