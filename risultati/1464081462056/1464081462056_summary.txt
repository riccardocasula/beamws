#input=1464081462056
#input_size=180
#background=autoBG
#background_size=250
#meanLength=175.72375690607734
#meanStructureContent=0.5846923821092214
#bin=33

#motif=1
gggggggpppppppggggggg*

#qBEAR
aaaaaaaxxxxxxxaaaaaaa*

pvalue=0.009527176580637398
score=1558.491978636771
coverage=0.7222222222222222
fall-out=0.04
runtime(s)=44.65

#motif=2
eeee[bb[affffffmmmmffffff]a]bbeeeee

#qBEAR
zzzzbzzbzaaaaaaxxxxaaaaaabzbzzzzzzz

pvalue=0.0019325607105366016
score=1603.2131786616285
coverage=0.36
fall-out=0.016
runtime(s)=45.28
