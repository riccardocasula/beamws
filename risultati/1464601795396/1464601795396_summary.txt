#input=1464601795396
#input_size=14
#background=back_fastb
#background_size=14
#meanLength=155.57142857142858
#meanStructureContent=0.5921872836148304
#bin=33

#motif=1
***********ppppppp************

#qBEAR
z*zzzzz*zzzxxxxxxx*zz*zzzz*zzz

pvalue=0.42160781086231003
score=132.45039120100833
coverage=0.9285714285714286
fall-out=0.9285714285714286
runtime(s)=10.15

#motif=2
b*a******mmmm******]a*bb*****

#qBEAR
z*zaazzzaxxxxaaaaaazzczzcc*zz

pvalue=0.3281693350201347
score=82.38317849200911
coverage=0.7857142857142857
fall-out=0.7857142857142857
runtime(s)=6.05
