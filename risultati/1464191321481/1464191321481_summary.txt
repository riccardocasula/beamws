#input=1464191321481
#input_size=12359
#background=back_fastb
#background_size=12358
#meanLength=132.65846751355286
#meanStructureContent=0.4388557872712628
#bin=31

#motif=1
*********************

#qBEAR
**zzz*sssssssssssszzz

pvalue=0.11744311740479851
score=1683.556575258829
coverage=0.38902823853062546
fall-out=0.376436316556077
runtime(s)=67.59

#motif=2
*d*oooooo***

#qBEAR
zzzxxxxxxzzz

pvalue=0.3300130326363807
score=667.2528167638625
coverage=0.6974579015544041
fall-out=0.7140313966661272
runtime(s)=49.77
