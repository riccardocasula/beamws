iiiiiiiiihhhhhhhhmmmmhhhhhhhh>>>>	chr1:4763354-4763474_bg$1	120	41	74	11.3309
ddd%%%%%%hhhhhhhhmmmmhhhhhhhh6666	SeqCat03|0_bg$1	170	131	164	8.2272
%ggggggg!!a[eeeeennnnneeeee]a5555	SeqCat03|1_bg$1	170	56	89	10.1647
dd!!ggggggg[eeeeemmmmeeeee666666g	SeqCat01|5_bg$1	185	8	41	9.6663
eeeccc[dddd[ddddnnnnnddddddddccc3	SeqCat03|6_bg$1	170	69	102	8.5107
H:dddd[ccc[ddddbblllbb]dddd]ccc]d	SeqCat03|7_bg$1	170	134	167	6.9060
ddddccc[hhhhhhhhnnnnnhhhhhhhh]ccc	SeqCat03|8_bg$1	170	129	162	10.2129
[eeeee[bb[affffffmmmmffffff]a]bbe	SeqCat01|12_bg$1	185	106	139	10.4527
!eeeee[bb[affffffmmmmffffff]a]bbe	SeqCat01|13_bg$1	185	90	123	10.5326
!ddddbb[dddd[bb[aooooooa]bb]dddd]	SeqCat03|14_bg$1	170	49	82	7.8013
ddddccc[hhhhhhhhnnnnnhhhhhhhh]ccc	SeqCat03|8$1	170	129	162	10.2129
dd!!ggggggg[eeeeemmmmeeeee666666g	SeqCat01|5$1	185	8	41	9.6663
[eeeee[bb[affffffmmmmffffff]a]bbe	SeqCat01|12$1	185	106	139	10.4527
iiiiiiiiihhhhhhhhmmmmhhhhhhhh>>>>	chr1:4763354-4763474$1	120	41	74	11.3309
!eeeee[bb[affffffmmmmffffff]a]bbe	SeqCat01|13$1	185	90	123	10.5326
eeeccc[dddd[ddddnnnnnddddddddccc3	SeqCat03|6$1	170	69	102	8.5107
H:dddd[ccc[ddddbblllbb]dddd]ccc]d	SeqCat03|7$1	170	134	167	6.9060
ddd%%%%%%hhhhhhhhmmmmhhhhhhhh6666	SeqCat03|0$1	170	131	164	8.2272
%ggggggg!!a[eeeeennnnneeeee]a5555	SeqCat03|1$1	170	56	89	10.1647
!ddddbb[dddd[bb[aooooooa]bb]dddd]	SeqCat03|14$1	170	49	82	7.8013

#otherMatches

#Seq PSSM
UAGAAGCCGUGGUAUGGGAGAUGUACUAUAGCU	SeqCat03|8$1	su1	129	162	10.2129
UUAGUUCUACCAGCGCCGUACGGUGUCCAGGGG	SeqCat01|5$1	su1	8	41	9.6663
CGACGUACUUCAUAGGAUCAUUUCUAUAGUAGA	SeqCat01|12$1	su1	106	139	10.4527
AAAAUGUAUUUUUUAGCUCCUGCUAAAAACAAU	chr1:4763354-4763474$1	su1	41	74	11.3309
AGACGUACUUCAUAGGAUCAUUUCUAUAGUAGA	SeqCat01|13$1	su1	90	123	10.5326
UGCCUGCUCAAACCGGUGAUUUUGGUUGACGGA	SeqCat03|6$1	su1	69	102	8.5107
GGACUCCUCACGAUUCAGCUUGUAAUCAUGAUG	SeqCat03|7$1	su1	134	167	6.9060
GCUAAUACGUUACUAGGUGACCUUAGUAAAAUC	SeqCat03|0$1	su1	131	164	8.2272
GCUCCGCGUCGCGCUAGGCAUUCUGGCACAAUC	SeqCat03|1$1	su1	56	89	10.1647
AGGCAAGUGCCAGUCCGACAGUCCUGAAUGGUU	SeqCat03|14$1	su1	49	82	7.8013

#PSSM
d : 0.30	! : 0.20	i : 0.10	% : 0.10	e : 0.10	H : 0.10	[ : 0.10	
d : 0.39	e : 0.30	i : 0.10	g : 0.10	: : 0.10	
d : 0.39	e : 0.30	i : 0.10	g : 0.10	! : 0.10	
d : 0.30	e : 0.20	i : 0.10	% : 0.10	g : 0.10	! : 0.10	c : 0.10	
g : 0.20	c : 0.20	d : 0.20	e : 0.20	i : 0.10	% : 0.10	
g : 0.20	c : 0.20	e : 0.20	i : 0.10	% : 0.10	d : 0.10	b : 0.10	
[ : 0.39	g : 0.20	i : 0.10	% : 0.10	c : 0.10	b : 0.10	
g : 0.20	[ : 0.20	b : 0.20	i : 0.10	% : 0.10	d : 0.10	c : 0.10	
d : 0.20	b : 0.20	i : 0.10	% : 0.10	! : 0.10	g : 0.10	c : 0.10	h : 0.10	
h : 0.30	d : 0.20	[ : 0.20	! : 0.10	g : 0.10	c : 0.10	
h : 0.30	a : 0.30	d : 0.20	g : 0.10	[ : 0.10	
h : 0.30	[ : 0.30	d : 0.20	f : 0.20	
h : 0.30	e : 0.20	d : 0.20	f : 0.20	[ : 0.10	
h : 0.30	e : 0.20	d : 0.20	f : 0.20	b : 0.10	
h : 0.30	e : 0.20	d : 0.20	f : 0.20	b : 0.10	
h : 0.30	e : 0.20	f : 0.20	d : 0.10	b : 0.10	[ : 0.10	
h : 0.20	e : 0.20	n : 0.20	f : 0.20	b : 0.10	a : 0.10	
m : 0.49	n : 0.30	l : 0.10	o : 0.10	
m : 0.49	n : 0.30	l : 0.10	o : 0.10	
m : 0.49	n : 0.30	l : 0.10	o : 0.10	
m : 0.49	n : 0.30	b : 0.10	o : 0.10	
h : 0.30	f : 0.20	n : 0.10	e : 0.10	d : 0.10	b : 0.10	o : 0.10	
h : 0.30	e : 0.20	f : 0.20	d : 0.10	] : 0.10	o : 0.10	
h : 0.30	e : 0.20	d : 0.20	f : 0.20	a : 0.10	
h : 0.30	e : 0.20	d : 0.20	f : 0.20	] : 0.10	
h : 0.30	e : 0.20	d : 0.20	f : 0.20	b : 0.10	
h : 0.30	d : 0.20	f : 0.20	e : 0.10	6 : 0.10	b : 0.10	
] : 0.49	h : 0.30	6 : 0.10	d : 0.10	
h : 0.30	a : 0.30	d : 0.20	6 : 0.10	c : 0.10	
] : 0.30	6 : 0.20	c : 0.20	> : 0.10	5 : 0.10	d : 0.10	
c : 0.30	6 : 0.20	b : 0.20	> : 0.10	5 : 0.10	d : 0.10	
6 : 0.20	c : 0.20	b : 0.20	> : 0.10	5 : 0.10	] : 0.10	d : 0.10	
e : 0.20	> : 0.10	6 : 0.10	5 : 0.10	g : 0.10	3 : 0.10	d : 0.10	c : 0.10	] : 0.10	

#score	0.0
#seq	20
#width	33
#escape	true
#onStep	-1
#minSteps	-1
#maxSteps	-1