#input=1464001436801
#input_size=178
#background=autoBG
#background_size=250
#meanLength=175.72375690607734
#meanStructureContent=0.5846923821092214
#bin=33

#motif=1
gggggggpppppppggggggg

#qBEAR
aaaaaaaxxxxxxxaaaaaaa

pvalue=0.0040411166690829115
score=1564.35988579957
coverage=0.7303370786516854
fall-out=0.024
runtime(s)=56.6
