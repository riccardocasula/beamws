ff[dddd[cccpppppppcccddddffff	chr1:4763788-4763908_bg$1	120	61	90	14.3106
!!bb!!eeeeepppppppeeeee22bb22	chr1:4763892-4764012_bg$1	120	79	108	16.4208
bb!!gggggggpppppppggggggg22bb	SeqCat03|0_bg$1	170	16	45	16.7805
!!bbgggggggpppppppggggggg4444	SeqCat03|1_bg$1	170	4	33	17.8452
eeee!!eeeeepppppppeeeeeeeeee:	SeqCat01|5_bg$1	185	121	150	16.1077
ddd[gggggggpppppppgggggggdddd	SeqCat03|6_bg$1	170	20	49	16.1860
H==========ppppppp==========:	SeqCat03|7_bg$1	170	8	37	16.2082
FFF:gggggggpppppppggggggg:::e	SeqCat03|8_bg$1	170	4	33	13.2881
eeee!!eeeeerrrrrrrrreeeee333e	SeqCat01|12_bg$1	185	35	64	14.1886
fffff[ddddqqqqqqqqddddffffffa	SeqCat01|13_bg$1	185	55	84	7.3183
ff[dddd[cccpppppppcccddddffff	chr1:4763788-4763908$1	120	61	90	14.3106
eeee!!eeeeepppppppeeeeeeeeee:	SeqCat01|5$1	185	121	150	16.1077
H==========ppppppp==========:	SeqCat03|7$1	170	8	37	16.2082
bb!!gggggggpppppppggggggg22bb	SeqCat03|0$1	170	16	45	16.7805
FFF:gggggggpppppppggggggg:::e	SeqCat03|8$1	170	4	33	13.2881
ddd[gggggggpppppppgggggggdddd	SeqCat03|6$1	170	20	49	16.1860
eeee!!eeeeerrrrrrrrreeeee333e	SeqCat01|12$1	185	35	64	14.1886
!!bb!!eeeeepppppppeeeee22bb22	chr1:4763892-4764012$1	120	79	108	16.4208
!!bbgggggggpppppppggggggg4444	SeqCat03|1$1	170	4	33	17.8452
fffff[ddddqqqqqqqqddddffffffa	SeqCat01|13$1	185	55	84	7.3183

#otherMatches

#Seq PSSM
caUgcUccaUggUcUcUacaUgagcUgcU	chr1:4763788-4763908$1	su1	61	90	14.3106
AUCGUUAAGAUCAGUCUUGUCUUUGAUAU	SeqCat01|5$1	su1	121	150	16.1077
CGUGAUGGAAGAGUAAGCCUUCCAUCACG	SeqCat03|7$1	su1	8	37	16.2082
GUAUAUGGAAGAGUAAGCCUUCCAUCCGC	SeqCat03|0$1	su1	16	45	16.7805
GUCAAUGGAAGAGUAAGCCUUCCAUCUUC	SeqCat03|8$1	su1	4	33	13.2881
GCACAUGGAAGAGUAAGCCUUCCAUUGCG	SeqCat03|6$1	su1	20	49	16.1860
AGGGGUGGCGUCAUUCCUUUACGCUUUGC	SeqCat01|12$1	su1	35	64	14.1886
UcaUggUgcUggUUaUagcagUaaaaUcc	chr1:4763892-4764012$1	su1	79	108	16.4208
UCGUAUGGAAGAGUAAGCCUUCCAUUUCA	SeqCat03|1$1	su1	4	33	17.8452
AACAGAGUACGCGCCCGAGUGCCUGUUCG	SeqCat01|13$1	su1	55	84	7.3183

#PSSM
f : 0.20	! : 0.20	e : 0.20	b : 0.10	d : 0.10	H : 0.10	F : 0.10	
f : 0.20	! : 0.20	e : 0.20	b : 0.10	d : 0.10	= : 0.10	F : 0.10	
b : 0.20	e : 0.20	[ : 0.10	! : 0.10	d : 0.10	= : 0.10	F : 0.10	f : 0.10	
b : 0.20	e : 0.20	d : 0.10	! : 0.10	[ : 0.10	= : 0.10	: : 0.10	f : 0.10	
g : 0.39	! : 0.30	d : 0.10	= : 0.10	f : 0.10	
g : 0.39	! : 0.30	d : 0.10	= : 0.10	[ : 0.10	
g : 0.39	e : 0.30	d : 0.20	= : 0.10	
g : 0.39	e : 0.30	[ : 0.10	= : 0.10	d : 0.10	
g : 0.39	e : 0.30	c : 0.10	= : 0.10	d : 0.10	
g : 0.39	e : 0.30	c : 0.10	= : 0.10	d : 0.10	
g : 0.39	e : 0.30	c : 0.10	= : 0.10	q : 0.10	
p : 0.80	r : 0.10	q : 0.10	
p : 0.80	r : 0.10	q : 0.10	
p : 0.80	r : 0.10	q : 0.10	
p : 0.80	r : 0.10	q : 0.10	
p : 0.80	r : 0.10	q : 0.10	
p : 0.80	r : 0.10	q : 0.10	
p : 0.80	r : 0.10	q : 0.10	
g : 0.39	e : 0.20	c : 0.10	= : 0.10	r : 0.10	d : 0.10	
g : 0.39	e : 0.20	c : 0.10	= : 0.10	r : 0.10	d : 0.10	
g : 0.39	e : 0.30	c : 0.10	= : 0.10	d : 0.10	
g : 0.39	e : 0.30	d : 0.20	= : 0.10	
g : 0.39	e : 0.30	d : 0.10	= : 0.10	f : 0.10	
g : 0.39	e : 0.20	d : 0.10	2 : 0.10	= : 0.10	f : 0.10	
g : 0.39	e : 0.20	d : 0.10	2 : 0.10	= : 0.10	f : 0.10	
f : 0.20	b : 0.10	2 : 0.10	4 : 0.10	e : 0.10	d : 0.10	= : 0.10	: : 0.10	3 : 0.10	
f : 0.20	b : 0.10	2 : 0.10	4 : 0.10	e : 0.10	d : 0.10	= : 0.10	: : 0.10	3 : 0.10	
f : 0.20	2 : 0.10	b : 0.10	4 : 0.10	e : 0.10	d : 0.10	= : 0.10	: : 0.10	3 : 0.10	
: : 0.20	e : 0.20	f : 0.10	2 : 0.10	b : 0.10	4 : 0.10	d : 0.10	a : 0.10	

#score	0.0
#seq	20
#width	29
#escape	true
#onStep	-1
#minSteps	-1
#maxSteps	-1