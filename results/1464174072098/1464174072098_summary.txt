#input=1464174072098
#input_size=177
#background=autoBG
#background_size=250
#meanLength=175.72375690607734
#meanStructureContent=0.5846923821092214
#bin=33

#motif=1
gggggggpppppppggggggg

#qBEAR
aaaaaaaxxxxxxxaaaaaaa

pvalue=0.008576239923928863
score=1488.1078058897558
coverage=0.7344632768361582
fall-out=0.036
runtime(s)=56.24

#motif=2
eeee[bb[affffffmmmmffffff]a]bbeeeee

#qBEAR
zzzzbzzbzaaaaaaxxxxaaaaaabzbzzzzzzz

pvalue=0.002929612997819464
score=1563.1963886249882
coverage=0.34911242603550297
fall-out=0.024
runtime(s)=81.79
