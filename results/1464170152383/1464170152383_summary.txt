#input=1464170152383
#input_size=180
#background=autoBG
#background_size=250
#meanLength=175.72375690607734
#meanStructureContent=0.5846923821092214
#bin=33

#motif=1
gggggggpppppppggggggg

#qBEAR
aaaaaaaxxxxxxxaaaaaaa

pvalue=0.005642524610895316
score=1626.224666966731
coverage=0.7111111111111111
fall-out=0.032
runtime(s)=39.39

#motif=2
eeee[bb[affffffmmmmffffff]a]bbeeeee

#qBEAR
zzzzbzzbzaaaaaaxxxxaaaaaabzbzzzzzzz

pvalue=0.0024326869183931565
score=1569.1715235924949
coverage=0.34269662921348315
fall-out=0.02
runtime(s)=65.11
