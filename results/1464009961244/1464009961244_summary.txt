#input=1464009961244
#input_size=178
#background=autoBG
#background_size=250
#meanLength=175.72375690607734
#meanStructureContent=0.5846923821092214
#bin=33

#motif=1
gggggggpppppppggggggg

#qBEAR
aaaaaaaxxxxxxxaaaaaaa

pvalue=0.005224162153813272
score=1569.2306745104036
coverage=0.7134831460674157
fall-out=0.036
runtime(s)=40.0

#motif=2
eeeee[bb[affffffmmmmffffff]a]bbeeee

#qBEAR
zzzzzbzzbzaaaaaaxxxxaaaaaabzbzzzzzz

pvalue=0.002686404818390775
score=1427.2768330921383
coverage=0.39644970414201186
fall-out=0.016
runtime(s)=70.09
