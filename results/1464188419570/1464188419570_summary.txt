#input=1464188419570
#input_size=67
#background=autoBG
#background_size=250
#meanLength=303.24050632911394
#meanStructureContent=0.5268975176382888
#bin=32

#motif=1
ffffffmmmmffffff:

#qBEAR
aaaaaaxxxxaaaaaat

pvalue=0.07053618161021147
score=711.3415586591542
coverage=0.9850746268656716
fall-out=0.164
runtime(s)=46.08

#motif=2
********oooooo******

#qBEAR
zzaa*zzzxxxxxxzzzaaa

pvalue=0.07493792307273339
score=452.45237930679434
coverage=0.9824561403508771
fall-out=0.172
runtime(s)=46.15
