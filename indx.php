<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BEAM Motif finder</title>
<link href="css/myStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
 <div id="header" align="center"><a href="/index.php"><img src="images/logoBeam.png" alt="logo" name="logo" width="500" height="94" /></a></div>
<div id="navigation" align="center"><a class="menu" href="/index.php" target="_self">Download</a>  <a class="menu" href="/documentation.php" target="_self">Documentation</a><a class="menuEnd" href="/contacts.php" target="_self">Contacts</a> </div>
<div id="main">
	<h1>BEAM suite</h1>
	<b>BEAM</b> is a new motif finder tool exploiting a new encoding for RNA secondary structure and a substitution matrix of RNA structural elements to look for the best secondary structure motif in a set of unaligned but related RNAs.
	<a class="downloadlink" href="download/BEAM_release_1.4.8.jar"><b>BEAM1.4.8 release</b></a> - Secondary structure motif finder software (requires <a class="downloadlink" href="https://www.java.com/it/download/">Java 1.6+</a>)
	<br>
	<a class="downloadlink" href="download/BeaREncoder.jar"><b>BEAR Encoder</b></a> - BEAR Encoder (<a class="downloadlink" href="documentation/readmeEncoder.txt">docs</a>)
</div>
<div id="footer"><img style="float:left; vertical-align:central; padding-left:5px; padding-top:2px" src="images/logoTorVergat.png" width="42" height="55" alt="logoTV" /><div style="padding-top:20px;">© 2016 Marco Pietrosanto, Centre for Molecular Bioinformatics, University of Rome "Tor Vergata".</div></div></div>

</body>
</html>
